function out=WTRBLN(P,Pet,Dt,Kc,Runoff,Watercapacity,button,mode)
%inputs
if isempty(mode);mode='script';end
%Select the Standard Procedure(S) or the Succesive Approximation
%procedure(A)
if strcmp(button,'SP')
    Dry_options=false;
else
    Dry_options=true;
end
L_month=0;
if Dry_options
    while L_month<1 || L_month>12
        L_month = str2num(cell2mat(inputdlg('Enter Last Month of Wet Period (value 1-12)')));
    end
end

%prepare flag, Indicating presence or absence of direct runoff
knt=0;
for i=1:12
    if Dt(i,1)==0
        knt=knt+1;
    end
end
if knt==12
    Flag_drnf=0;
else
    Flag_drnf=1;
end

%prepare flag, indicating crop coefficients deviating from 1
knt=0;
for i=1:12
    if Kc(i,1)==1
        knt=knt+1;
    end
end
if knt==12
    Flag_kc=0; %All crop coefficients are equal to 1
else
    Flag_kc=1; %At least one of the crop coeff is not equal to 1
end

%Calculate Precipitation minus direct Runoff if Flag_drnf=1
if Flag_drnf
    Pr=P-Dt;
else
    Pr=P;
end

%Calculate Actual Crop potential Evapotranspiration if Flag_kc=1
if Flag_kc==1
    Petr=Kc.*Pet; %int
else
    Petr=Pet;
end

%Calculate the effective difference between precipitation and Pot
%Evapotranspiration
Pr_petr=Pr-Petr;
%Calculate Yearly Values of P,Dt,Pr,Pet,Petr and Pr_petr
P(13,1)=sum(P(1:12,1));
Dt(13,1)=sum(Dt(1:12,1));
Pr(13,1)=sum(Pr(1:12,1));
Pet(13,1)=sum(Pet(1:12,1));
Petr(13,1)=sum(Petr(1:12,1));
Pr_petr(13,1)=sum(Pr_petr(1:12,1));

%Test for Climate with constant moisture deficit
index_d=0;
for i=1:12
    if Pr_petr(i)<=0
        index_d=index_d+1;
    end
end

if index_d~=12 %see line 1045

    %Calculate Accumulated Potential Water Loss for Each Deficit Period
    Acpwl=zeros(13,1);
    for Rpt=1:2
        for i=1:12
            if Pr_petr(i)<0
                Acpwl(i)=Acpwl(max((2-i)*12,i-1))+Pr_petr(i);
            end
        end
    end

    %@@@@Additional outer loop to consider the "goto again:" line 1130-1325
    outerflag=false;
    while ~outerflag
        if Dry_options
            %Successive Approximation Procedure is Applied
            index=L_month;                  %Introduce a potential Water loss
            Acpwl(index)=Acpwl(index)-1;    %in the Last month of wet period
            exitflag=false;
            while ~exitflag
                index=index+1;
                if index>12;index=1;end
                if Pr_petr(index)>=0
                    %Needs further check Line1170 May need break instead of exit
                    break %exitflag=true;
                end
                Acpwl(index)=Acpwl(index)-1; 
            end
        else
            outerflag=true;
        end

        %Calculate Soi Moisture
        Sm=zeros(13,1);
        %Determine soil moisture for the months with Pr_potr<0
        for i=1:12
            if Acpwl(i)<0
                Sm(i)=Watercapacity*exp(Acpwl(i)/Watercapacity); %int
            end
        end

        %determine soil moisture for the months with Pr_potr>=0
        for Rpt=1:2
            for i=1:12
                if Acpwl(i)>=0
                    Sm(i)=Sm(max((2-i)*12,i-1))+Pr_petr(i);
                    if Sm(i)>Watercapacity;Sm(i)=Watercapacity;end
                end
            end
        end

        if Dry_options
            %End of Succesive Approximations?
            if Sm(max((2-L_month)*12,L_month-1))+Pr_petr(L_month)-Sm(L_month)>=0
                outerflag=true;
            end
        end
    end
    %Calculate Change in Soil Moisture, Actual Evapostranspiration, Soil
    %Moisture Deficit, and Moisture Surplus.
    for i=1:12
        Dsm(i,1)=Sm(i)-Sm(max((2-i)*12,i-1));
        if Pr_petr(i)<0
            Aet(i,1)=Pr(i)+abs(Dsm(i));
        else
            Aet(i,1)=Petr(i);
        end
        D(i,1)=Petr(i)-Aet(i);
        if Sm(i)==Watercapacity
            S(i,1)=Pr_petr(i)-Dsm(i);
        else
            S(i,1)=0;
        end
    end

    %Calculation of Runoff
    T=zeros(13,1);
    Ro=zeros(13,1);
    Det=zeros(13,1);
    %Rot=zeros(13,1);
    Nsrpls_per=0;
    for i=1:12
        if S(max((2-i)*12,i-1))<=0 && S(i)>0
            Nsrpls_per=Nsrpls_per+1;
            Frst_srpls_mnth(Nsrpls_per)=i;
        end
    end

    if Nsrpls_per~=0 && Nsrpls_per~=1
        Smax=S(Frst_srpls_mnth(1));
        Max_pointer=Frst_srpls_mnth(1);
        for i=2:12
            if S(Frst_srpls_mnth(i))>Smax
                Smax=S(Frst_srpls_mnth(i));
                Max_pointer=Frst_srpls_mnth(i);
            end
        end
        Flag=Max_pointer;
    end
    if Nsrpls_per==1
        Flag=Frst_srpls_mnth(1);
    end
    if Nsrpls_per~=0
        T(Flag)=S(Flag);
        Ro(Flag)=Runoff/100*T(Flag); %int
        Det(Flag)=T(Flag)-Ro(Flag);
        Index=Flag;
        for i=1:11
            Index=Index+1;
            if Index>12;Index=1;end
            T(Index)=S(Index)+Det(max((2-Index)*12,Index-1));
            Ro(Index)=Runoff/100*T(Index); %int
            Det(Index)=T(Index)-Ro(Index);
        end
    end

    Rot=Ro+Dt;
    %Calculate Yearly Valuesof Actual Evapotranspiration, Soil Moisture
    %Deficit, Moisture Surplus, Runoff without direct Runoff and Runoff
    %including direct runoff
    Aet(13,1)=sum(Aet(1:12,1));
    D(13,1)=sum(D(1:12,1));
    S(13,1)=sum(S(1:12,1));
    Ro(13,1)=sum(Ro(1:12,1));
    Rot(13,1)=sum(Rot(1:12,1));

    %Determine the number of wet periods and final month of each wet period.
    %Prepare a warning in case the water capacity is not reached at the end of
    %wet period(s)
    Nwet=0;
    for i=1:12
        if Pr_petr(max((2-i)*12,i-1))>=0 && Pr_petr(i)<0
            Nwet=Nwet+1;
            Final_month(Nwet)=max((2-i)*12,i-1);
        end
    end
    for i=1:Nwet
        if Sm(Final_month(i))<Watercapacity && strcmp(button,'SP')
            Warn(i)=1;
            warndlg('You must run using SAP')
            out=[];
            return 
        end
    end
end %if index_d~=12 %see line 1045
out.Acpwl=Acpwl;
out.Aet=Aet;
out.D=D;
out.Det=Det;
out.Dsm=Dsm;
out.Dt=Dt;
out.Kc=Kc;
out.P=P;
out.Pet=Pet;
out.Petr=Petr;
out.Pr_petr=Pr_petr;
out.Ro=Ro;
out.Rot=Rot;
out.S=S;
out.Sm=Sm;
















    
    
