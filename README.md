# WTRBLN #

### Program Description ###

WTRBLN is a computer program that calculates water balance based on the 
basis of long-term average monthly precipitation, potential 
evapotranspiration and vegetation characteristics, according to the 
method of Thornthwaite and Matter. The program is based on the work of 
Donker 1987. The paper includes an appendix that provides the source 
code that was written in Basic 3.0. This version is written in Matlab

### How do I get set up? ###

* Dependencies

	To run this program you need Matlab. The GUI does not run under octave, 
	yet there is a non gui command that should work.
	
* How to run tests
	
	There are two input file examples. A more detailed description can 
	be found under [ this link ](http://subsurface.gr/software/wtrbln/)                            
 

### Who do I talk to? ###

giorgk@gmail.com
