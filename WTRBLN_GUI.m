function varargout = WTRBLN_GUI(varargin)
%Current version 1.1
%%%%%%%%%%%%%%%%%%%History%%%%%%%%%%%%%%%
%19/06/08 version 1.1 Add legends and record versions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%WTRBLN_GUI M-file for WTRBLN_GUI.fig
%      WTRBLN_GUI, by itself, creates a new WTRBLN_GUI or raises the existing
%      singleton*.
%
%      H = WTRBLN_GUI returns the handle to a new WTRBLN_GUI or the handle to
%      the existing singleton*.
%
%      WTRBLN_GUI('Property','Value',...) creates a new WTRBLN_GUI using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to WTRBLN_GUI_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      WTRBLN_GUI('CALLBACK') and WTRBLN_GUI('CALLBACK',hObject,...) call the
%      local function named CALLBACK in WTRBLN_GUI.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help WTRBLN_GUI

% Last Modified by GUIDE v2.5 09-Apr-2008 12:43:47

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @WTRBLN_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @WTRBLN_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before WTRBLN_GUI is made visible.
function WTRBLN_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

% Choose default command line output for WTRBLN_GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes WTRBLN_GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = WTRBLN_GUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit8_Callback(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit8 as text
%        str2double(get(hObject,'String')) returns contents of edit8 as a double


% --- Executes during object creation, after setting all properties.
function edit8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit9_Callback(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit9 as text
%        str2double(get(hObject,'String')) returns contents of edit9 as a double


% --- Executes during object creation, after setting all properties.
function edit9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%(get(hObject,'Value') == get(hObject,'Max'))
if get(handles.checkbox1,'Value')
    %Read input file
    filename=get(handles.edit6,'String');
    P=textread(filename,'%f',12,'headerlines',2);
    Pet=textread(filename,'%f',12,'headerlines',4);
    Dt=textread(filename,'%f',12,'headerlines',6);
    Kc=textread(filename,'%f',12,'headerlines',8);
    Runoff=textread(filename,'%f',1,'headerlines',10);
    Watercapacity=textread(filename,'%f',1,'headerlines',12);
else
    %Create file
    P=strread(get(handles.edit1,'String'));P=P(:);
    Pet=strread(get(handles.edit2,'String'));Pet=Pet(:);
    Dt=strread(get(handles.edit3,'String'));Dt=Dt(:);
    Kc=strread(get(handles.edit4,'String'));Kc=Kc(:);
    line=get(handles.edit5,'String');
    filename=get(handles.edit6,'String');
    Runoff=str2double(get(handles.edit8,'String'));
    Watercapacity=str2double(get(handles.edit9,'String'));
    %Generate the file
    fid = fopen(filename,'w+');
    fprintf(fid,[line '\n']);
    fprintf(fid,'PRECIPITATION\n');
    fprintf(fid,[num2str(P') '\n']);
    fprintf(fid,'REFERENCE POTENCIAL EVAPOTRANSIRATION\n');
    fprintf(fid,[num2str(Pet') '\n']);
    fprintf(fid,'DIRECT RUNOFF\n');
    fprintf(fid,[num2str(Dt') '\n']);
    fprintf(fid,'CROP COEFFICIENTS\n');
    fprintf(fid,[num2str(Kc') '\n']);
    fprintf(fid,'AVERAGE MONTHLY RUNOFF\n');
    fprintf(fid,[num2str(Runoff) '\n']);
    fprintf(fid,'WATER CAPACITY OF ROOTZONE\n');
    fprintf(fid,[num2str(Watercapacity) '\n']);
    fclose(fid)
end

if ~get(handles.checkbox2,'Value')
    %Calculate Water Balance
    button = questdlg('Standard(SP) or Successive Approximation(SAP) Procedure?                      (First try SP. In case of warning switch to SAP)','title','SP','SAP','SP');
    mode='GUI';
    out=WTRBLN(P,Pet,Dt,Kc,Runoff,Watercapacity,button,mode);
    assignin('base', 'out', out)
    if ~isempty(out)
        makeplot(out)
    end
    
end
    


% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2

function makeplot(out)
figure(2);hold on
for i=1:12
    if out.P(i)<out.Aet(i)
        figure(2);rectangle('Position',[i,out.P(i,1),1,out.Aet(i,1)-out.P(i,1)],'LineStyle','none','FaceColor',[0.9,1,0.4])
    end
    if out.P(i)>out.Petr(i)
        figure(2);rectangle('Position',[i,out.Petr(i,1),1,out.P(i,1)-out.Petr(i,1)],'LineStyle','none','FaceColor',[0,0.8,0.5])
    end
    if out.Petr(i)>out.Aet(i)
        figure(2);rectangle('Position',[i,out.Aet(i,1),1,out.Petr(i,1)-out.Aet(i,1)],'LineStyle','none','FaceColor',[0,0,0.9])
    end
    if out.S(i)>0
        figure(2);rectangle('Position',[i,out.P(i,1)-out.S(i,1),1,out.S(i,1)],'LineStyle','none','FaceColor',[0.9,0,0])
    end
    
end
set(gca,'YLimMode','auto')
minmaxaxis=get(gca,'YLim');

figure(2);stairs([out.P(1:12);out.P(12)],'LineWidth',1.5)
figure(2);stairs([out.Petr(1:12);out.Petr(12)],'--r','LineWidth',1.5)
figure(2);stairs([out.Aet(1:12);out.Aet(12)],':g','LineWidth',1.5)
figure(2);stairs([out.Rot(1:12);out.Rot(12)],'-.k','LineWidth',1.5)
figure(2);axis([1,13,minmaxaxis(1),minmaxaxis(2)+4*minmaxaxis(2)*0.1/2])
set(gca,'XTick',1.5:12.5)
set(gca,'XTickLabel',{'J','F','M','A','M','J','J','A','S','O','N','D'})
xlabel('Month');
ylabel('Water depth [mm]')
set(gca,'OuterPosition',[0 0 1 1]);

text(1.1,minmaxaxis(2)+3*minmaxaxis(2)*0.1/2,'Moisture Surplus')
text(1.1,minmaxaxis(2)+1*minmaxaxis(2)*0.1/2,'Moisture deficit')
text(5,minmaxaxis(2)+3*minmaxaxis(2)*0.1/2,'Soil moisture recharge')
text(5,minmaxaxis(2)+1*minmaxaxis(2)*0.1/2,'Soil moisture utilization')
rectangle('Position',[4,minmaxaxis(2)+2.4*minmaxaxis(2)*0.1/2,0.8,minmaxaxis(2)*0.1/2],'LineStyle','none','FaceColor',[0.9,0,0])
rectangle('Position',[4,minmaxaxis(2)+0.4*minmaxaxis(2)*0.1/2,0.8,minmaxaxis(2)*0.1/2],'LineStyle','none','FaceColor',[0,0,0.9])
rectangle('Position',[9,minmaxaxis(2)+2.4*minmaxaxis(2)*0.1/2,0.8,minmaxaxis(2)*0.1/2],'LineStyle','none','FaceColor',[0,0.8,0.5])
rectangle('Position',[9,minmaxaxis(2)+0.4*minmaxaxis(2)*0.1/2,0.8,minmaxaxis(2)*0.1/2],'LineStyle','none','FaceColor',[0.9,1,0.4])

text(11,minmaxaxis(2)+3*minmaxaxis(2)*0.1/2,'P-DRO')
text(10,minmaxaxis(2)+0*minmaxaxis(2)*0.1/2,'CROP POT.EVP.')
text(11,minmaxaxis(2)+2*minmaxaxis(2)*0.1/2,'ACT. EVP.')
text(11,minmaxaxis(2)+1*minmaxaxis(2)*0.1/2,'RUNOFF')
line([10 11],[minmaxaxis(2)+3*minmaxaxis(2)*0.1/2 minmaxaxis(2)+3*minmaxaxis(2)*0.1/2])
line([10 11],[minmaxaxis(2)+2*minmaxaxis(2)*0.1/2 minmaxaxis(2)+2*minmaxaxis(2)*0.1/2],'LineStyle','--','Color','g','LineWidth',1.5)
line([10 11],[minmaxaxis(2)+1*minmaxaxis(2)*0.1/2 minmaxaxis(2)+1*minmaxaxis(2)*0.1/2],'LineStyle','-.','Color','k','LineWidth',1.5)    
line([9 10],[minmaxaxis(2)+0*minmaxaxis(2)*0.1/2 minmaxaxis(2)+0*minmaxaxis(2)*0.1/2],'LineStyle',':','Color','r','LineWidth',1.5)   

figure(2);hold off





